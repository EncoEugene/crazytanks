#pragma once

#include <iostream>

using namespace std;

class CaptionProg
{
public:
	void Caption()
	{
		cout << "###########################" << endl;
		cout << "#                         #" << endl;
		cout << "# ******   *   **  * *  * #" << endl;
		cout << "#   **    * *  **  * * ** #" << endl;
		cout << "#   **    ***  * * * **   #" << endl;
		cout << "#   **   *   * * * * **   #" << endl;
		cout << "#   **   *   * *  ** * ** #" << endl;
		cout << "#   **   *   * *  ** *  * #" << endl;
		cout << "#                         #" << endl;
		cout << "###########################" << endl;
	}

	void Win(int score) {
		cout << endl << endl << "Ви перемогли, набравши - " << score << endl;
	}

	void GameOver(int score) {
		cout << endl << endl << "Ви програли, так як вийшов час!" << endl << "Ващ рахунок: " << score << endl;
	}
};