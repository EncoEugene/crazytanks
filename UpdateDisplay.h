#pragma once

#include <windows.h>

class UpdateDisplay
{
private:
	int x, y;
public:
	// Мигание экрана
	void setcur(int x, int y)
	{
		COORD coord;
		coord.X = x;
		coord.Y = y;
		SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), coord);
	};
};