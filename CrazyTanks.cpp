// CrazyTanks.cpp: определяет точку входа для консольного приложения.
//

#include "stdafx.h"
#include "HeaderFile\CaptionP.h"
#include "HeaderFile\Peremen.h"
#include "HeaderFile\Enemy.h"
#include "HeaderFile\Setup.h"
#include "HeaderFile\Draw.h"
#include "HeaderFile\Input.h"
#include "HeaderFile\Logic.h"


int main()
{
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	
	CaptionProg caption;
	EnemyCount enemy;
	SetupOp setup;
	DrawDis draw;
	Control control;
	LogicElements logic;

	caption.Caption();

	clock_t start;
	double duration;

	while (true)
	{
		cout << endl << "Введіть кількість ворогів: ";
		cin >> countEnemy;
		if (countEnemy < 5)
		{
			setup.Setup();
			start = std::clock();
			break;
		}
		else
			cout << "Введіть максимум 4 ворога!" << endl << endl;
	}

	while (!lose)
	{
		enemy.Enemy();
		draw.Draw();
		control.Input();
		logic.Logic();

		if (score == 30)
			break;
		
		duration = (std::clock() - start) / (double)CLOCKS_PER_SEC;

		if (duration > 100.0)
			break;
		cout << "Time: " << duration;
		Sleep(150);
	}

	duration = (std::clock() - start) / (double)CLOCKS_PER_SEC;

	if (score == 30)
		caption.Win(score);
	if (duration > 100.0)
		caption.GameOver(score);

	cout << endl << "Пройшло часу: " << duration << " сек" << endl;

	delete[] enemyX;
	delete[] enemyY;
	delete[] randPosEnemy;
	delete[] posXEnemy;
	delete[] posYEnemy;

	system("pause");
	return 0;
}

